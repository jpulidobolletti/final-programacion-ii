var seleccionTabla;
var editmode = false;

$('#TurnosDatatable').find('tr').click( function(){
    seleccionTabla = $(this).find('td:first').text();
    if(editmode){
        let dateTime = $(this).find('td:eq(5)').text()
        let arr = dateTime.split('T')
        let fecha = arr[0]
        let hora = arr[1]

        $('#animalEdit').val($(this).find('td:eq(1)').text());
        $('#nombreMascotaEdit').val($(this).find('td:eq(2)').text());
        $('#nombreDuenioEdit').val($(this).find('td:eq(3)').text());
        $('#numeroContactoEdit').val($(this).find('td:eq(4)').text());
        $('#veterinarioEdit').val($(this).find('td:eq(6)').text());
        $('#fechaEdit').val(fecha);
        $('#horaEdit').val(hora);
    }
});

function highlight_row() {
    var table = document.getElementById('TurnosDatatable');
    var cells = table.getElementsByTagName('td');

    for (var i = 0; i < cells.length; i++) {
        // Take each cell
        var cell = cells[i];
        // do something on onclick event for cell
        cell.onclick = function () {
            // Get the row id where the cell exists
            var rowId = this.parentNode.rowIndex;

            var rowsNotSelected = table.getElementsByTagName('tr');
            for (var row = 0; row < rowsNotSelected.length; row++) {
                rowsNotSelected[row].style.backgroundColor = "";
                rowsNotSelected[row].classList.remove('selected');
            }
            var rowSelected = table.getElementsByTagName('tr')[rowId];
            rowSelected.style.backgroundColor = "lightblue";
            rowSelected.className += " selected";
        }
    }
}

window.onload = highlight_row;

//accion del boton ingresar
$(document).ready(function() {
    $('#accionIngresar').click(function () {
        let fecha = $('#fecha').val()
        let hora = $('#hora').val()
        $.ajax({
            type: "POST",
            url: "/api/v1/turnos",
            data: {
                "animal": $('#animal').val(),
                "nombreMascota": $('#nombreMascota').val(),
                "nombreDuenio": $('#nombreDuenio').val(),
                "numeroContacto": $('#numeroContacto').val(),
                "veterinario": $('#veterinario').val(),
                "fecha": fecha + ' ' + hora,

            },
            success: function (result) {
                console.log(result);
                location.reload();
            },
            error: function (e) {
                $('#error').show();            }
        })
    });
});

//accion del boton editar
$(document).ready(function() {
    $('#accionEditar').click(function() {
        if(seleccionTabla != null){
            let fecha = $('#fechaEdit').val()
            let hora = $('#horaEdit').val()
            $.ajax({
                type : "PUT",
                url : "/api/v1/turnos/" + seleccionTabla,
                data: {
                    "animal": $('#animalEdit').val(),
                    "nombreMascota": $('#nombreMascotaEdit').val(),
                    "nombreDuenio": $('#nombreDuenioEdit').val(),
                    "numeroContacto": $('#numeroContactoEdit').val(),
                    "veterinario": $('#veterinarioEdit').val(),
                    "fecha": fecha + ' ' + hora,
                },
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (e) {
                    $('#error').show();
                }
            })
        }else{
            $('#errorNoSeleccion').show();
        }
    });
});

//accion del boton eliminar
$(document).ready(function() {
    $('#accionEliminar').click(function() {
        if(seleccionTabla != null){
            $.ajax({
                type : "DELETE",
                url : "/api/v1/turnos/" + seleccionTabla,
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (e) {
                    $('#error').show();
                }
            })
        }else{
            $('#errorNoSeleccion').show();
        }
    });
});

//accion del boton buscar
function buscarTurnoById() {
    var url = '/api/v1/turnos';
    if ($('#searchId').val() != '') {
        url = url + '/' + $('#searchId').val();
    }
    $("#resultsBlock").load(url);
}
