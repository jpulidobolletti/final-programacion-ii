var seleccionTabla;
var editmode = false;

$('#ProductosDatatable').find('tr').click( function(){
    seleccionTabla = $(this).find('td:first').text();
    if(editmode){
        $('#idedit').val(seleccionTabla);
        $('#categoriaEdit').val($(this).find('td:eq(1)').text());
        $('#descripcionEdit').val($(this).find('td:eq(2)').text());
        $('#precioEdit').val($(this).find('td:eq(3)').text());
    }
});

function highlight_row() {
    var table = document.getElementById('ProductosDatatable');
    var cells = table.getElementsByTagName('td');

    for (var i = 0; i < cells.length; i++) {
        // Take each cell
        var cell = cells[i];
        // do something on onclick event for cell
        cell.onclick = function () {
            // Get the row id where the cell exists
            var rowId = this.parentNode.rowIndex;

            var rowsNotSelected = table.getElementsByTagName('tr');
            for (var row = 0; row < rowsNotSelected.length; row++) {
                rowsNotSelected[row].style.backgroundColor = "";
                rowsNotSelected[row].classList.remove('selected');
            }
            var rowSelected = table.getElementsByTagName('tr')[rowId];
            rowSelected.style.backgroundColor = "lightblue";
            rowSelected.className += " selected";
        }
    }
}

window.onload = highlight_row;

//accion del boton editar
$(document).ready(function() {
    $('#accionEditar').click(function() {
        if(seleccionTabla != null){
            console.log(seleccionTabla);
            $.ajax({
                type : "PUT",
                url : "/api/v1/productos/" + seleccionTabla,
                data: {
                    "categoria": $('#categoriaEdit').val(),
                    "descripcion": $('#descripcionEdit').val(),
                    "precio": parseFloat($('#precioEdit').val()),
                },
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (e) {
                    console.log(e);
                }
            })
        }else{
            $('#errorNoSeleccion').show();
        }
    });
});

//accion del boton eliminar
$(document).ready(function() {
    $('#accionEliminar').click(function() {
        if(seleccionTabla != null){
            $.ajax({
                type : "DELETE",
                url : "/api/v1/productos/" + seleccionTabla,
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (e) {
                    console.log(e);
                }
            })
        }else{
            $('#errorNoSeleccion').show();
        }
    });
});

//accion del boton buscar
function buscarProductoById() {
    var url = '/api/v1/productos/desc';
    if ($('#searchId').val() != '') {
        url = url + '/' + $('#searchId').val();
    }
    $("#resultsBlock").load(url);
}
