var seleccionTabla;
var editmode = false;

$('#UsuariosDatatable').find('tr').click( function(){
    seleccionTabla = $(this).find('td:first').text();
    if(editmode){
        $('#usuarioEdit').val($(this).find('td:eq(0)').text());
        $('#passwordEdit').val($(this).find('td:eq(1)').text());
        $('#tipoEdit').val($(this).find('td:eq(2)').text());
    }
});

function highlight_row() {
    var table = document.getElementById('UsuariosDatatable');
    var cells = table.getElementsByTagName('td');

    for (var i = 0; i < cells.length; i++) {
        // Take each cell
        var cell = cells[i];
        // do something on onclick event for cell
        cell.onclick = function () {
            // Get the row id where the cell exists
            var rowId = this.parentNode.rowIndex;

            var rowsNotSelected = table.getElementsByTagName('tr');
            for (var row = 0; row < rowsNotSelected.length; row++) {
                rowsNotSelected[row].style.backgroundColor = "";
                rowsNotSelected[row].classList.remove('selected');
            }
            var rowSelected = table.getElementsByTagName('tr')[rowId];
            rowSelected.style.backgroundColor = "lightblue";
            rowSelected.className += " selected";
        }
    }
}

window.onload = highlight_row;

$(document).ready(function() {
    let user = $('#usuarioEdit').val()
    $('#accionEditar').click(function() {
        if(seleccionTabla != null){
            $.ajax({
                type : "PUT",
                url : "/api/v1/usuarios/" + seleccionTabla,
                data: {
                    "usuario": $('#usuarioEdit').val(),
                    "password": $('#passwordEdit').val(),
                    "tipo": $('#tipoEdit').val(),
                },
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (e) {
                    $('#error').show();
                    console.log(e);
                }
            })
        }else{
            $('#errorNoSeleccion').show();
        }
    });
});

//accion del boton eliminar
$(document).ready(function() {
    $('#accionEliminar').click(function() {
        if(seleccionTabla != null){
            $.ajax({
                type : "DELETE",
                url : "/api/v1/usuarios/" + seleccionTabla,
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (e) {
                    console.log(e);
                }
            })
        }else{
            $('#errorNoSeleccion').show();
        }
    });
});

//accion del boton buscar
function buscarUsuarioById() {
    var url = '/api/v1/usuarios';
    if ($('#searchId').val() != '') {
        url = url + '/' + $('#searchId').val();
    }
    $("#resultsBlock").load(url);
}