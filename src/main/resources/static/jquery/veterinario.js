let seleccionTabla;
let editmode = false;
const diasElementsEdit = ["#LUNESEdit", "#MARTESEdit", "#MIERCOLESEdit",
    "#JUEVESEdit", "#VIERNESEdit", "#SABADOEdit"]
const diasElements = ["#LUNES", "#MARTES", "#MIERCOLES",
    "#JUEVES", "#VIERNES", "#SABADO"]
const especialidadesElementsEdit = ["#GATOEdit", "#PERROEdit", "#TORTUGAEdit", "#CANARIOEdit"]
const especialidadesElements = ["#GATO", "#PERRO", "#TORTUGA", "#CANARIO"]

$('#VeterinariosDatatable').find('tr').click( function(){
    let dias = $(this).find('td:eq(2)').text().split(',')
    let especialidades = $(this).find('td:eq(3)').text().split(',')
    seleccionTabla = $(this).find('td:first').text();
    if(editmode){
        $('#usuarioEdit').val($(this).find('td:eq(0)').text());
        $('#passwordEdit').val($(this).find('td:eq(1)').text());
        dias.forEach(d => $("#"+d+"Edit").prop('checked', true))
        especialidades.forEach(e => $("#"+e+"Edit").prop('checked', true))
    }
});

function highlight_row() {
    var table = document.getElementById('VeterinariosDatatable');
    var cells = table.getElementsByTagName('td');

    for (var i = 0; i < cells.length; i++) {
        // Take each cell
        var cell = cells[i];
        // do something on onclick event for cell
        cell.onclick = function () {
            // Get the row id where the cell exists
            var rowId = this.parentNode.rowIndex;

            var rowsNotSelected = table.getElementsByTagName('tr');
            for (var row = 0; row < rowsNotSelected.length; row++) {
                rowsNotSelected[row].style.backgroundColor = "";
                rowsNotSelected[row].classList.remove('selected');
            }
            var rowSelected = table.getElementsByTagName('tr')[rowId];
            rowSelected.style.backgroundColor = "lightblue";
            rowSelected.className += " selected";
        }
    }
}

window.onload = highlight_row;

$(document).ready(function() {
    let maxDias = [];
    let dias = ""
    let especialidades = ""
    $('#accionIngresar').click(function() {
        diasElements.forEach(d => {
            if($(d).is(":checked")){
                maxDias.push(d)
                console.log(d)
                console.log(d.substring(1))
                dias = dias + d.substring(1) + ","
            }
        })
        dias = dias.slice(0,-1)
        if (maxDias.length > 3){
            alert("El veterinario solo puede atender hasta 3 dias")
            maxDias = []
            dias = ""
            return
        }
        especialidadesElements.forEach(e => {
            if($(e).is(":checked")){
                especialidades = especialidades + e.substring(1) + ","
            }
        })
        $.ajax({
            type : "POST",
            url : "/api/v1/veterinarios",
            data: {
                "usuario": $('#usuario').val(),
                "password": $('#password').val(),
                "dias": dias,
                "especialidades": especialidades,
            },
            success: function (result) {
                console.log(result);
                location.reload();
            },
            error: function (e) {
                console.log(e);
            }
        })
    })
    maxDias = []
    dias = ""
    especialidades = ""
});

$(document).ready(function() {
    let maxDias = [];
    let dias = ""
    let especialidades = ""
    $('#accionEditar').click(function() {
        if(seleccionTabla != null){
            diasElementsEdit.forEach(d => {
                if($(d).is(":checked")){
                    maxDias.push(d)
                    dias = dias + d.slice(1,-4) + ","
                }
            })
            dias = dias.slice(0,-1)
            if (maxDias.length > 3){
                alert("El veterinario solo puede atender hasta 3 dias")
                maxDias = []
                dias = ""
                return
            }
            especialidadesElementsEdit.forEach(e => {
                if($(e).is(":checked")){
                    especialidades = especialidades + e.slice(1,-4) + ","
                }
            })
            $.ajax({
                type : "PUT",
                url : "/api/v1/veterinarios/" + seleccionTabla,
                data: {
                    "usuario": seleccionTabla,
                    "password": $('#passwordEdit').val(),
                    "dias": dias,
                    "especialidades": especialidades,
                },
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (e) {
                    console.log(e);
                }
            })
        }else{
            $('#errorNoSeleccion').show();
        }
        maxDias = []
        dias = ""
        especialidades = ""
    });
});

//accion del boton eliminar
$(document).ready(function() {
    $('#accionEliminar').click(function() {
        if(seleccionTabla != null){
            $.ajax({
                type : "DELETE",
                url : "/api/v1/veterinarios/" + seleccionTabla,
                success: function (result) {
                    console.log(result);
                    location.reload();
                },
                error: function (e) {
                    console.log(e);
                }
            })
        }else{
            $('#errorNoSeleccion').show();
        }
    });
});

//accion del boton buscar
function buscarVeterinarioById() {
    var url = '/api/v1/veterinarios';
    if ($('#searchId').val() != '') {
        url = url + '/' + $('#searchId').val();
    }
    $("#resultsBlock").load(url);
}