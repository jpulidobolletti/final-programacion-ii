-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2021 at 01:08 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `veterinaria`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(13);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) NOT NULL,
  `categoria` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `categoria`, `descripcion`, `precio`) VALUES
(0, 'MEDICAMENTO', 'Antipulgas y garrapatas', 300.5),
(1, 'REGULAR', 'Collar', 200),
(10, 'REGULAR', 'Correa', 100),
(11, 'MEDICAMENTO', 'Antibiotico', 500);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role`) VALUES
(1, 'ADMINISTRADOR'),
(2, 'VETERINARIO'),
(3, 'RECEPCIONISTA');

-- --------------------------------------------------------

--
-- Table structure for table `turnos`
--

CREATE TABLE `turnos` (
  `id` bigint(20) NOT NULL,
  `animal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `nombre_duenio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_mascota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_contacto` int(11) DEFAULT NULL,
  `veterinario_usuario` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `turnos`
--

INSERT INTO `turnos` (`id`, `animal`, `fecha`, `nombre_duenio`, `nombre_mascota`, `numero_contacto`, `veterinario_usuario`) VALUES
(0, 'PERRO', '2021-11-20 10:30:00', 'Leo Messi', 'pulga', 1198529385, 'vete'),
(9, 'PERRO', '2021-11-30 14:00:00', 'Albertito', 'Dylan', 1145454545, 'vete');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
('admin', 1),
('administrador', 1),
('rece', 3),
('recepcionista', 1),
('vete', 2),
('veterinaria', 2),
('veterinario', 2);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `password`, `tipo`) VALUES
('admin', '123', 'ADMINISTRADOR'),
('administrador', '123', 'ADMINISTRADOR'),
('rece', '123', 'RECEPCIONISTA'),
('recepcionista', '123', 'ADMINISTRADOR'),
('vete', '123', 'VETERINARIO'),
('veterinaria', '123', 'VETERINARIO'),
('veterinario', '123', 'VETERINARIO');

-- --------------------------------------------------------

--
-- Table structure for table `ventas`
--

CREATE TABLE `ventas` (
  `id` bigint(20) NOT NULL,
  `producto_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ventas`
--

INSERT INTO `ventas` (`id`, `producto_id`) VALUES
(5, 0),
(8, 1),
(12, 10);

-- --------------------------------------------------------

--
-- Table structure for table `veterinarios`
--

CREATE TABLE `veterinarios` (
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `veterinarios`
--

INSERT INTO `veterinarios` (`usuario`) VALUES
('vete'),
('veterinaria'),
('veterinario');

-- --------------------------------------------------------

--
-- Table structure for table `veterinario_dias_atiende`
--

CREATE TABLE `veterinario_dias_atiende` (
  `veterinario_usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dias_atiende` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `veterinario_dias_atiende`
--

INSERT INTO `veterinario_dias_atiende` (`veterinario_usuario`, `dias_atiende`) VALUES
('vete', 'JUEVES'),
('vete', 'VIERNES'),
('vete', 'SABADO'),
('veterinario', 'JUEVES'),
('veterinario', 'VIERNES'),
('veterinario', 'SABADO'),
('veterinaria', 'MARTES'),
('veterinaria', 'MIERCOLES'),
('veterinaria', 'JUEVES');

-- --------------------------------------------------------

--
-- Table structure for table `veterinario_especialidades`
--

CREATE TABLE `veterinario_especialidades` (
  `veterinario_usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `especialidades` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `veterinario_especialidades`
--

INSERT INTO `veterinario_especialidades` (`veterinario_usuario`, `especialidades`) VALUES
('vete', 'TORTUGA'),
('vete', 'CANARIO'),
('veterinario', 'GATO'),
('veterinario', 'PERRO'),
('veterinario', 'TORTUGA'),
('veterinaria', 'CANARIO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKs5vnao2xsm439k0436gu84qsf` (`veterinario_usuario`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario`);

--
-- Indexes for table `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKm2vj8hr3u07dh7lf45hq7tman` (`producto_id`);

--
-- Indexes for table `veterinarios`
--
ALTER TABLE `veterinarios`
  ADD PRIMARY KEY (`usuario`);

--
-- Indexes for table `veterinario_dias_atiende`
--
ALTER TABLE `veterinario_dias_atiende`
  ADD KEY `FKnjwhdaiul5g60cuxa4yan734a` (`veterinario_usuario`);

--
-- Indexes for table `veterinario_especialidades`
--
ALTER TABLE `veterinario_especialidades`
  ADD KEY `FKfqdyrod2x12had5xvn9b83mjf` (`veterinario_usuario`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `turnos`
--
ALTER TABLE `turnos`
  ADD CONSTRAINT `FKs5vnao2xsm439k0436gu84qsf` FOREIGN KEY (`veterinario_usuario`) REFERENCES `veterinarios` (`usuario`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`),
  ADD CONSTRAINT `FKdb77ocp4vrjv2qx01rjf35iuy` FOREIGN KEY (`user_id`) REFERENCES `usuarios` (`usuario`);

--
-- Constraints for table `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `FKm2vj8hr3u07dh7lf45hq7tman` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`);

--
-- Constraints for table `veterinarios`
--
ALTER TABLE `veterinarios`
  ADD CONSTRAINT `FKr4ficft7vjn2uibrv5r3qfmf9` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`usuario`);

--
-- Constraints for table `veterinario_dias_atiende`
--
ALTER TABLE `veterinario_dias_atiende`
  ADD CONSTRAINT `FKnjwhdaiul5g60cuxa4yan734a` FOREIGN KEY (`veterinario_usuario`) REFERENCES `veterinarios` (`usuario`);

--
-- Constraints for table `veterinario_especialidades`
--
ALTER TABLE `veterinario_especialidades`
  ADD CONSTRAINT `FKfqdyrod2x12had5xvn9b83mjf` FOREIGN KEY (`veterinario_usuario`) REFERENCES `veterinarios` (`usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
