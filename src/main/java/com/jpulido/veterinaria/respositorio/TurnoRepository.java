package com.jpulido.veterinaria.respositorio;

import com.jpulido.veterinaria.modelo.dao.TurnoDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TurnoRepository extends JpaRepository<TurnoDAO,Long> {
}
