package com.jpulido.veterinaria.respositorio;

import com.jpulido.veterinaria.modelo.dao.UsuarioDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioDAO, String> {
}
