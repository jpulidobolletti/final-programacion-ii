package com.jpulido.veterinaria.respositorio;

import com.jpulido.veterinaria.modelo.dao.VeterinarioDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VeterinarioRepository extends JpaRepository<VeterinarioDAO,String> {
}
