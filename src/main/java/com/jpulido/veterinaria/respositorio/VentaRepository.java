package com.jpulido.veterinaria.respositorio;

import com.jpulido.veterinaria.modelo.dao.VentaDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VentaRepository extends JpaRepository<VentaDAO,Long> {
}
