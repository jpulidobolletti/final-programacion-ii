package com.jpulido.veterinaria.respositorio;

import com.jpulido.veterinaria.modelo.dao.ProductoDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;//si trae null no revienta

@Repository
public interface ProductoRepository extends JpaRepository<ProductoDAO,Long> {

    @Query("SELECT p FROM ProductoDAO p WHERE p.descripcion = ?1 ")
    Optional<ProductoDAO> findByDescripcion(String descripcion);

}
