package com.jpulido.veterinaria.controlador;

import com.jpulido.veterinaria.modelo.dao.VentaDAO;
import com.jpulido.veterinaria.modelo.dto.VentaDTO;
import com.jpulido.veterinaria.modelo.dto.VeterinarioDTO;
import com.jpulido.veterinaria.servicio.ProductoService;
import com.jpulido.veterinaria.servicio.VentaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("api/v1/ventas")
public class VentaController {

    private final VentaService ventaService;
    private final ProductoService productoService;

    public VentaController(VentaService ventaService, ProductoService productoService) {
        this.ventaService = ventaService;
        this.productoService = productoService;
    }

    @GetMapping(produces = "application/json")
    public String getVentas(Model model, HttpServletRequest request) {
        model.addAttribute("ventas",ventaService.findAllVentas());
        if (request.isUserInRole("ROLE_VETERINARIO")){
            model.addAttribute("productos",productoService.findAllProductos());
        } else {
            model.addAttribute("productos",productoService.findAllProductosRegulares());
        }
        return "api/ventas";
    }

    @PostMapping(produces = "application/json")
    public String createVenta(@ModelAttribute(name="VentaForm") VentaDTO venta) {
        ventaService.createVenta(venta);
        return "redirect:ventas";
    }

    @DeleteMapping(value="/{id}", produces = "application/json")
    public ResponseEntity<String> deleteVenta(@PathVariable Long id) {
        ventaService.deleteVenta(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
