package com.jpulido.veterinaria.controlador;

import com.jpulido.veterinaria.modelo.dto.VeterinarioDTO;
import com.jpulido.veterinaria.servicio.VeterinarioService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/v1/veterinarios")
public class VeterinarioController {

    private final VeterinarioService veterinarioService;

    public VeterinarioController(VeterinarioService veterinarioService) {
        this.veterinarioService = veterinarioService;
    }

    @GetMapping(value="/{id}", produces = "application/json")
    public String getVeterinarioById(@PathVariable String id, Model model) {
        model.addAttribute("veterinarios",veterinarioService.findVeterinarioById(id));
        return "fragments/resultSearchVeterinario :: resultsList";
    }

    @GetMapping()
    public String getVeterinarios(Model model) {
        model.addAttribute("veterinarios", veterinarioService.findAllVeterinarios());
        return "api/veterinarios";
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<String> updateVeterinario(@PathVariable String id, @ModelAttribute(name="VeterinarioForm") VeterinarioDTO veterinario) {
        veterinarioService.updateVeterinario(id, veterinario);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping()
    public String createVeterinario(@ModelAttribute(name="VeterinarioForm") VeterinarioDTO veterinario) {
        veterinarioService.createVeterinario(veterinario);
        return "redirect:veterinarios";
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<String> deleteVeterinario(@PathVariable String id) {
        veterinarioService.deleteVeterinario(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
