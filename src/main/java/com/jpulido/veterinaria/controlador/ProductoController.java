package com.jpulido.veterinaria.controlador;

import com.jpulido.veterinaria.modelo.dto.ProductoDTO;
import com.jpulido.veterinaria.servicio.ProductoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/v1/productos")
public class ProductoController {

    private final ProductoService productoService;

    public ProductoController(ProductoService productoService) {
        this.productoService = productoService;
    }

    @GetMapping(value="/{id}", produces = "application/json")
    public String getProductoById(@PathVariable Long id, Model model) {
        model.addAttribute("productos",productoService.findProductoById(id));
        return "fragments/resultSearchProducto :: resultsList";
    }

    @GetMapping(value="/desc/{desc}", produces = "application/json")
    public String getProductoById(@PathVariable String desc, Model model) {
        model.addAttribute("productos",productoService.findProductoByDescripcion(desc));
        return "fragments/resultSearchProducto :: resultsList";
    }

    @GetMapping()
    public String getProductos(Model model) {
        model.addAttribute("productos", productoService.findAllProductos());
        return "api/productos";
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable Long id, @ModelAttribute(name="ProductoForm") ProductoDTO producto) {
        productoService.updateProducto(id, producto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping()
    public String createProducto(@ModelAttribute(name="ProductoForm") ProductoDTO producto) {
        productoService.createProducto(producto);
        return "redirect:productos";
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable Long id) {
        productoService.deleteProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
