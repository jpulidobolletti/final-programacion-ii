package com.jpulido.veterinaria.controlador;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jpulido.veterinaria.modelo.dto.TurnoDTO;
import com.jpulido.veterinaria.servicio.TurnoService;
import com.jpulido.veterinaria.servicio.VeterinarioService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalTime;

@Controller
@RequestMapping("api/v1/turnos")
public class TurnoController {

    private final TurnoService turnoService;
    private final VeterinarioService veterinarioService;

    public TurnoController(TurnoService turnoService, VeterinarioService veterinarioService) {
        this.turnoService = turnoService;
        this.veterinarioService = veterinarioService;
    }

    @GetMapping(value="/{id}", produces = "application/json")
    public String getTurnoById(@PathVariable Long id, Model model) {
        model.addAttribute("turnos",turnoService.findTurnoById(id));
        return "fragments/resultSearchTurno :: resultsList";
    }

    @GetMapping()
    public String getTurnos(Model model) {
        model.addAttribute("turnos", turnoService.findAllTurnos());
        model.addAttribute("veterinarios", veterinarioService.findAllVeterinarios());
        return "api/turnos";
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<String> updateTurno(@PathVariable Long id, @ModelAttribute(name="TurnoForm") TurnoDTO turno) {
        if(turno.getFecha().toLocalTime().isAfter(LocalTime.of(11,59)) &&
                turno.getFecha().toLocalTime().isBefore(LocalTime.of(13,31))){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(turnoService.updateTurno(id, turno)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping()
    @JsonFormat(pattern="dd-MM-yyyy")
    public ResponseEntity<String> createTurno(@ModelAttribute(name="TurnoForm") TurnoDTO turno) {
        if(turno.getFecha().toLocalTime().isAfter(LocalTime.of(11,59)) &&
                turno.getFecha().toLocalTime().isBefore(LocalTime.of(13,31))){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(turnoService.createTurno(turno))  return new ResponseEntity<>(HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<String> deleteTurno(@PathVariable Long id) {
        turnoService.deleteTurno(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
