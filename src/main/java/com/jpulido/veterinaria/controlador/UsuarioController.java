package com.jpulido.veterinaria.controlador;

import com.jpulido.veterinaria.modelo.dto.UsuarioDTO;
import com.jpulido.veterinaria.servicio.UsuarioService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/v1/usuarios")
public class UsuarioController {

    private final UsuarioService usuarioService;

    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @GetMapping(value="/{id}", produces = "application/json")
    public String getUsuarioById(@PathVariable String id, Model model) {
        model.addAttribute("usuarios",usuarioService.findUsuarioById(id));
        return "fragments/resultSearchUsuario :: resultsList";
    }

    @GetMapping()
    public String getUsuarios(Model model) {
        model.addAttribute("usuarios", usuarioService.findAllUsuarios());
        return "api/usuarios";
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<String> updateUsuario(@PathVariable String id, @ModelAttribute(name="UsuarioForm") UsuarioDTO usuario) {
        usuarioService.updateUsuario(id, usuario);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping()
    public String createUsuario(@ModelAttribute(name="UsuarioForm") UsuarioDTO usuario) {
        usuarioService.createUsuario(usuario);
        return "redirect:usuarios";
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<String> deleteUsuario(@PathVariable String id) {
        usuarioService.deleteUsuario(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
