package com.jpulido.veterinaria.controlador;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @GetMapping("/admin/home")
    public String adminHome() {
        return "/admin/home";
    }

    @GetMapping("/veterinario/home")
    public String investigadorHome() {
        return "/veterinario/home";
    }

    @GetMapping("/recepcionista/home")
    public String recepcionistaHome() {
        return "/recepcionista/home";
    }

    @GetMapping("/")
    public String homeAlternativo() {
        return "home";
    }

    @GetMapping("/acercade")
    public String acercaDe() {
        return "acercade";
    }

    @GetMapping("/login")
    public String login() {
        return "home";
    }

}
