package com.jpulido.veterinaria.config;

import com.jpulido.veterinaria.respositorio.UsuarioRepository;
import com.jpulido.veterinaria.servicio.CustomUserDetailsService;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@EnableJpaRepositories(basePackageClasses = UsuarioRepository.class)
@Configuration
public class BasicSecurityConfig extends WebSecurityConfigurerAdapter {

    private final CustomUserDetailsService userDetailsService;

    BasicSecurityConfig(CustomUserDetailsService userDetailsService){
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userDetailsService)
                .passwordEncoder(getPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.headers().frameOptions().disable();
        http.authorizeRequests()
                .antMatchers("/api/v1/productos").hasRole("ADMINISTRADOR")
                .antMatchers("/api/v1/productos/**").hasRole("ADMINISTRADOR")
                .antMatchers("/api/v1/usuarios").hasRole("ADMINISTRADOR")
                .antMatchers("/api/v1/usuarios/**").hasRole("ADMINISTRADOR")
                .antMatchers("/api/v1/veterinarios").hasRole("ADMINISTRADOR")
                .antMatchers("/api/v1/veterinarios/**").hasRole("ADMINISTRADOR")
                .antMatchers("/api/v1/turnos").hasRole("RECEPCIONISTA")
                .antMatchers("/api/v1/turnos/**").hasRole("RECEPCIONISTA")
                .antMatchers("/api/**").hasAnyRole("RECEPCIONISTA", "VETERINARIO")
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/home?logout=true");
    }

    private PasswordEncoder getPasswordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return charSequence.toString();
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return charSequence.toString().equals(s);
            }
        };
    }
}