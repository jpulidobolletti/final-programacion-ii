package com.jpulido.veterinaria.modelo.dto;

public class VeterinarioDTO {

    private String usuario;
    private String password;
    private String dias;
    private String especialidades;

    public VeterinarioDTO(String usuario, String password, String dias, String especialidades) {
        this.usuario = usuario;
        this.password = password;
        this.dias = dias;
        this.especialidades = especialidades;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    public String getEspecialidades() {
        return especialidades;
    }

    public void setEspecialidades(String especialidades) {
        this.especialidades = especialidades;
    }
}
