package com.jpulido.veterinaria.modelo.dto;

public class VentaDTO {

    private Long id;
    private Long productoId;
    private String producto;

    public VentaDTO(Long id, Long productoId, String producto) {
        this.id = id;
        this.productoId = productoId;
        this.producto = producto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductoId() {
        return productoId;
    }

    public void setProductoId(Long productoId) {
        this.productoId = productoId;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }
}
