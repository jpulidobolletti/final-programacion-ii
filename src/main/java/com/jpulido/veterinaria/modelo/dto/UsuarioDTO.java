package com.jpulido.veterinaria.modelo.dto;

import com.jpulido.veterinaria.modelo.dao.Role;
import com.jpulido.veterinaria.modelo.dao.enums.TipoUsuario;

import java.util.Set;

public class UsuarioDTO {

    private String usuario;
    private String password;
    private TipoUsuario tipo;
    private Set<Role> roles;

    public UsuarioDTO(String usuario, String password, TipoUsuario tipo, Set<Role> roles) {
        this.usuario = usuario;
        this.password = password;
        this.tipo = tipo;
        this.roles = roles;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TipoUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
