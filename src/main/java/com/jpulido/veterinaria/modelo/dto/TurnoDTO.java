package com.jpulido.veterinaria.modelo.dto;

import com.jpulido.veterinaria.modelo.dao.enums.Animal;

import java.time.LocalDateTime;

public class TurnoDTO {

    private Long id;
    private String nombreMascota;
    private String nombreDuenio;
    private Animal animal;
    private Integer numeroContacto;
    private String veterinario;
    private LocalDateTime fecha;

    public TurnoDTO(Long id, String nombreMascota, String nombreDuenio, Animal animal, Integer numeroContacto, String veterinario, LocalDateTime fecha) {
        this.id = id;
        this.nombreMascota = nombreMascota;
        this.nombreDuenio = nombreDuenio;
        this.animal = animal;
        this.numeroContacto = numeroContacto;
        this.veterinario = veterinario;
        this.fecha = fecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public String getNombreDuenio() {
        return nombreDuenio;
    }

    public void setNombreDuenio(String nombreDuenio) {
        this.nombreDuenio = nombreDuenio;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public Integer getNumeroContacto() {
        return numeroContacto;
    }

    public void setNumeroContacto(Integer numeroContacto) {
        this.numeroContacto = numeroContacto;
    }

    public String getVeterinario() {
        return veterinario;
    }

    public void setVeterinario(String veterinario) {
        this.veterinario = veterinario;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }
}
