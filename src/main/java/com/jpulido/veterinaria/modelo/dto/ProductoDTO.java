package com.jpulido.veterinaria.modelo.dto;

import com.jpulido.veterinaria.modelo.dao.enums.CategoriaProducto;

public class ProductoDTO {

    private final Long id;
    private final String descripcion;
    private final CategoriaProducto categoria;
    private final double precio;

    public ProductoDTO(Long id, String descripcion, CategoriaProducto categoria, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.precio = precio;
    }

    public Long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public CategoriaProducto getCategoria() {
        return categoria;
    }

    public double getPrecio() {
        return precio;
    }

}
