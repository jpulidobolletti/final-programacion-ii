package com.jpulido.veterinaria.modelo.dao.enums;

public enum CategoriaProducto {
    REGULAR,
    MEDICAMENTO
}
