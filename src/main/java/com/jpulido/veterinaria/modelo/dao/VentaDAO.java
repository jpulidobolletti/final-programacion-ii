package com.jpulido.veterinaria.modelo.dao;

import javax.persistence.*;

@Entity
@Table(name = "ventas")
public class VentaDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne
    private ProductoDAO producto;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public ProductoDAO getProducto() {
        return producto;
    }

    public void setProducto(ProductoDAO productoDAO) {
        this.producto = productoDAO;
    }
}
