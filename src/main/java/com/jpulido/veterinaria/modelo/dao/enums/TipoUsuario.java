package com.jpulido.veterinaria.modelo.dao.enums;

public enum TipoUsuario {
    ADMINISTRADOR,
    VETERINARIO,
    RECEPCIONISTA
}
