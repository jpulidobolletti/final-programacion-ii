package com.jpulido.veterinaria.modelo.dao;

import com.jpulido.veterinaria.modelo.dao.enums.Animal;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "turnos")
public class TurnoDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombreMascota;
    private String nombreDuenio;
    @Enumerated(EnumType.STRING)
    private Animal animal;
    private Integer numeroContacto;
    @OneToOne
    private VeterinarioDAO veterinario;
    private LocalDateTime fecha;

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombre) {
        this.nombreMascota = nombre;
    }

    public String getNombreDuenio() {
        return nombreDuenio;
    }

    public void setNombreDuenio(String nombreDuenio) {
        this.nombreDuenio = nombreDuenio;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public Integer getNumeroContacto() {
        return numeroContacto;
    }

    public void setNumeroContacto(Integer numeroContacto) {
        this.numeroContacto = numeroContacto;
    }

    public VeterinarioDAO getVeterinario() {
        return veterinario;
    }

    public void setVeterinario(VeterinarioDAO veterinario) {
        this.veterinario = veterinario;
    }
}
