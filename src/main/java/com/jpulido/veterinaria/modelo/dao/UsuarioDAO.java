package com.jpulido.veterinaria.modelo.dao;

import com.jpulido.veterinaria.modelo.dao.enums.TipoUsuario;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "usuarios")
@Inheritance(strategy = InheritanceType.JOINED)
public class UsuarioDAO {

    @Id
    private String usuario;
    private String password;
    @Enumerated(EnumType.STRING)
    private TipoUsuario tipo;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "usuario"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "role_id"))
    private Set<Role> roles;

    public UsuarioDAO() {
    }

    public UsuarioDAO(UsuarioDAO usuario) {
        this.roles = usuario.getRoles();
        this.usuario = usuario.getUsuario();
        this.password = usuario.getPassword();
        this.tipo = usuario.getTipo();
    }

    public TipoUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
