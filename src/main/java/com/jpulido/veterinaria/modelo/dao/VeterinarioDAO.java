package com.jpulido.veterinaria.modelo.dao;

import com.jpulido.veterinaria.modelo.dao.enums.Animal;
import com.jpulido.veterinaria.modelo.dao.enums.Dia;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "veterinarios")
@AttributeOverride(name="usuario", column=@Column(name="")) 
public class VeterinarioDAO extends UsuarioDAO {

    @ElementCollection @Enumerated(EnumType.STRING)
    @CollectionTable(name="veterinario_especialidades", joinColumns = @JoinColumn(name = "veterinario_usuario"))
    private List<Animal> especialidades;

    @ElementCollection @Enumerated(EnumType.STRING)
    @CollectionTable(name="veterinario_dias_atiende", joinColumns = @JoinColumn(name = "veterinario_usuario"))
    private List<Dia> diasAtiende;

    public List<Animal> getEspecialidades() {
        return especialidades;
    }

    public void setEspecialidades(List<Animal> especialidades) {
        this.especialidades = especialidades;
    }

    public List<Dia> getDiasAtiende() {
        return diasAtiende;
    }

    public void setDiasAtiende(List<Dia> diasAtiende) {
        this.diasAtiende = diasAtiende;
    }
}
