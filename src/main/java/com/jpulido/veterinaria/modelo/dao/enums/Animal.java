package com.jpulido.veterinaria.modelo.dao.enums;

public enum Animal {
    GATO,
    PERRO,
    TORTUGA,
    CANARIO
}
