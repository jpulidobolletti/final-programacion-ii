package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dto.TurnoDTO;

import java.util.List;

public interface TurnoService {
    TurnoDTO findTurnoById(Long id);
    List<TurnoDTO> findAllTurnos();
    boolean updateTurno(Long id, TurnoDTO turnoDTO);
    boolean createTurno(TurnoDTO turnoDTO);
    void deleteTurno(Long id);
}
