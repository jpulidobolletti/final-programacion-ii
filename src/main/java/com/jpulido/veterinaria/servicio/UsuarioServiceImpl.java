package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dao.Role;
import com.jpulido.veterinaria.modelo.dao.UsuarioDAO;
import com.jpulido.veterinaria.modelo.dto.UsuarioDTO;
import com.jpulido.veterinaria.respositorio.UsuarioRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    private final UsuarioRepository usuarioRepository;

    //spring busca constructor, le pasa el repositorio y se usa para consultas
    public UsuarioServiceImpl(UsuarioRepository UsuarioRepository) {
        this.usuarioRepository = UsuarioRepository;
    }

    @Override
    public UsuarioDTO findUsuarioById(String id) {
        return daoToDto(usuarioRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    @Override
    public List<UsuarioDTO> findAllUsuarios() {
        return usuarioRepository.findAll().stream().map(this::daoToDto).collect(Collectors.toList());
    }

    @Override
    public void updateUsuario(String id, UsuarioDTO usuarioDTO) {
        UsuarioDAO usuarioInicial; //trae usuario del repo y guarda dao
        usuarioInicial = usuarioRepository.findById(id).orElseThrow(RuntimeException::new);

        UsuarioDAO usuarioAGuardar = dtoToDao(usuarioDTO); //transforma la data a actualizar en dao y guarda
        usuarioAGuardar.setUsuario(usuarioInicial.getUsuario()); //sin pisar pk
        
        usuarioRepository.save(usuarioAGuardar); //guarda en repo
    }

    @Override
    public UsuarioDTO createUsuario(UsuarioDTO UsuarioDTO) {
        return daoToDto(usuarioRepository.save(dtoToDao(UsuarioDTO)));
    }//pasa el dto a dato, guarda en repo, vuelve a dto y retorna

    @Override
    public void deleteUsuario(String id) {
        usuarioRepository.deleteById(id);
    }

    private UsuarioDTO daoToDto(UsuarioDAO usuarioDAO){
        return new UsuarioDTO(
                usuarioDAO.getUsuario(),
                usuarioDAO.getPassword(),
                usuarioDAO.getTipo(),
                usuarioDAO.getRoles()
        );
    }

    private UsuarioDAO dtoToDao(UsuarioDTO usuarioDTO){
        UsuarioDAO usuario = new UsuarioDAO();
        Set<Role> roles = new HashSet<>(); //set: coleccion que no acepta repetidos.
        switch (usuarioDTO.getTipo()){
            case ADMINISTRADOR:
                roles.add(new Role(1, "ADMINISTRADOR"));
                break;
            case VETERINARIO:
                roles.add(new Role(2, "VETERINARIO"));
                break;
            case RECEPCIONISTA:
                roles.add(new Role(3, "RECEPCIONISTA"));
                break;
        }//
        usuario.setRoles(roles);
        usuario.setUsuario(usuarioDTO.getUsuario());
        usuario.setPassword(usuarioDTO.getPassword());
        usuario.setTipo(usuarioDTO.getTipo());
        return usuario;
    }
}

