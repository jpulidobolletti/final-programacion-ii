package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dto.ProductoDTO;

import java.util.List;

public interface ProductoService {
    ProductoDTO findProductoById(Long id);
    ProductoDTO findProductoByDescripcion(String descripcion);
    List<ProductoDTO> findAllProductos();
    List<ProductoDTO> findAllProductosRegulares();
    void updateProducto(Long id, ProductoDTO productoDTO);
    ProductoDTO createProducto(ProductoDTO productoDTO);
    void deleteProducto(Long id);
}
