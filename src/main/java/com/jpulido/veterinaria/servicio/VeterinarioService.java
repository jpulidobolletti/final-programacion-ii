package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dto.VeterinarioDTO;
import java.util.List;

public interface VeterinarioService {
    VeterinarioDTO findVeterinarioById(String id);
    List<VeterinarioDTO> findAllVeterinarios();
    void updateVeterinario(String id, VeterinarioDTO veterinarioDTO);
    VeterinarioDTO createVeterinario(VeterinarioDTO veterinarioDTO);
    void deleteVeterinario(String id);
}
