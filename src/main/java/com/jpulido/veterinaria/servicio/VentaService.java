package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dto.VentaDTO;

import java.util.List;

public interface VentaService {
    List<VentaDTO> findAllVentas();
    VentaDTO createVenta(VentaDTO ventaDTO);
    void deleteVenta(Long id);
}
