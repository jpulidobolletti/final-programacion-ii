package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dao.Role;
import com.jpulido.veterinaria.modelo.dao.VeterinarioDAO;
import com.jpulido.veterinaria.modelo.dao.enums.Animal;
import com.jpulido.veterinaria.modelo.dao.enums.Dia;
import com.jpulido.veterinaria.modelo.dao.enums.TipoUsuario;
import com.jpulido.veterinaria.modelo.dto.VeterinarioDTO;
import com.jpulido.veterinaria.respositorio.VeterinarioRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class VeterinarioServiceImpl implements VeterinarioService {

    private final VeterinarioRepository veterinarioRepository;

    public VeterinarioServiceImpl(VeterinarioRepository VeterinarioRepository) {
        this.veterinarioRepository = VeterinarioRepository;
    }

    @Override
    public VeterinarioDTO findVeterinarioById(String id) {
        return daoToDto(veterinarioRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    @Override
    public List<VeterinarioDTO> findAllVeterinarios() {
        return veterinarioRepository.findAll()
                .stream()
                .map(this::daoToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void updateVeterinario(String id, VeterinarioDTO veterinarioDTO) {
        VeterinarioDAO veterinario;
        veterinario = veterinarioRepository.findById(id).orElseThrow(RuntimeException::new);

        veterinario.getDiasAtiende().clear();
        veterinario.getEspecialidades().clear();
        setDias(veterinarioDTO, veterinario);
        setEspecialidades(veterinarioDTO, veterinario);

        veterinario.setUsuario(veterinarioDTO.getUsuario());
        veterinario.setPassword(veterinarioDTO.getPassword());
        veterinario.setTipo(TipoUsuario.VETERINARIO);
        veterinarioRepository.save(veterinario);
    }

    @Override
    public VeterinarioDTO createVeterinario(VeterinarioDTO VeterinarioDTO) {
        return daoToDto(veterinarioRepository.save(dtoToDao(VeterinarioDTO)));
    }

    @Override
    public void deleteVeterinario(String id) {
        veterinarioRepository.deleteById(id);
    }

    private VeterinarioDTO daoToDto(VeterinarioDAO veterinarioDAO){
        return new VeterinarioDTO(
                veterinarioDAO.getUsuario(),
                veterinarioDAO.getPassword(),
                veterinarioDAO.getDiasAtiende().stream().map(Enum::toString).collect(Collectors.joining(",")),
                veterinarioDAO.getEspecialidades().stream().map(Enum::toString).collect(Collectors.joining(","))
        );
    }

    private VeterinarioDAO dtoToDao(VeterinarioDTO veterinarioDTO){
        VeterinarioDAO veterinario = new VeterinarioDAO();
        Set<Role> roles = new HashSet<>();

        roles.add(new Role(2, "VETERINARIO"));
        veterinario.setRoles(roles);
        veterinario.setUsuario(veterinarioDTO.getUsuario());
        veterinario.setPassword(veterinarioDTO.getPassword());
        veterinario.setTipo(TipoUsuario.VETERINARIO);
        veterinario.setDiasAtiende(new ArrayList<>());
        veterinario.setEspecialidades(new ArrayList<>());
        setDias(veterinarioDTO, veterinario);
        setEspecialidades(veterinarioDTO, veterinario);
        return veterinario;
    }

    private void setDias(VeterinarioDTO veterinarioDTO, VeterinarioDAO veterinario) {
        String[] dias = veterinarioDTO.getDias().split(",");
        for (String d : dias) {
            veterinario.getDiasAtiende().add(Dia.valueOf(d));
        }
    }

    private void setEspecialidades(VeterinarioDTO veterinarioDTO, VeterinarioDAO veterinario) {
        String[] especialidades = veterinarioDTO.getEspecialidades().split(",");
        for (String e : especialidades) {
            veterinario.getEspecialidades().add(Animal.valueOf(e));
        }
    }
}

