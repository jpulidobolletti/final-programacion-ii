package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dao.VentaDAO;
import com.jpulido.veterinaria.modelo.dto.VentaDTO;
import com.jpulido.veterinaria.respositorio.ProductoRepository;
import com.jpulido.veterinaria.respositorio.VentaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VentaServiceImpl implements VentaService{

    private final VentaRepository ventaRepository;
    private final ProductoRepository productoRepository;

    public VentaServiceImpl(VentaRepository ventaRepository, ProductoRepository productoRepository) {
        this.ventaRepository = ventaRepository;
        this.productoRepository = productoRepository;
    }

    @Override
    public List<VentaDTO >findAllVentas() {
        return ventaRepository.findAll()
                .stream()
                .map(this::daoToDto)
                .collect(Collectors.toList());
    }


    @Override
    public VentaDTO createVenta(VentaDTO ventaDTO) {
        return daoToDto(ventaRepository.save(dtoToDao(ventaDTO)));
    }

    @Override
    public void deleteVenta(Long id) {
        ventaRepository.deleteById(id);
    }

    private VentaDTO daoToDto(VentaDAO ventaDAO) {
        return new VentaDTO(
                ventaDAO.getId(),
                ventaDAO.getProducto().getId(),
                ventaDAO.getProducto().getDescripcion()
        );
    }

    private VentaDAO dtoToDao(VentaDTO ventaDTO) {
        VentaDAO ventaDAO = new VentaDAO();
        ventaDAO.setId(ventaDTO.getId());
        ventaDAO.setProducto(productoRepository.findById(ventaDTO.getProductoId()).orElseThrow(RuntimeException::new));
        return ventaDAO;
    }

}
