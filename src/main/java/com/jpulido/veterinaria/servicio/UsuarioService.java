package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dto.UsuarioDTO;

import java.util.List;

public interface UsuarioService {
    UsuarioDTO findUsuarioById(String id);
    List<UsuarioDTO> findAllUsuarios();
    void updateUsuario(String id, UsuarioDTO usuarioDTO);
    UsuarioDTO createUsuario(UsuarioDTO usuarioDTO);
    void deleteUsuario(String id);
}
