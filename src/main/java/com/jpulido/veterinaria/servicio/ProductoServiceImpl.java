package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dao.ProductoDAO;
import com.jpulido.veterinaria.modelo.dao.enums.CategoriaProducto;
import com.jpulido.veterinaria.modelo.dto.ProductoDTO;
import com.jpulido.veterinaria.respositorio.ProductoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductoServiceImpl implements ProductoService {

    private final ProductoRepository productoRepository;

    public ProductoServiceImpl(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    @Override
    public ProductoDTO findProductoById(Long id) {
        return daoToDto(productoRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    public ProductoDTO findProductoByDescripcion(String descripcion){
        return daoToDto(productoRepository.findByDescripcion(descripcion).orElseThrow(RuntimeException::new));
    }

    @Override
    public List<ProductoDTO> findAllProductos() {
        return productoRepository.findAll()
                .stream()
                .map(this::daoToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductoDTO> findAllProductosRegulares() {
        return productoRepository.findAll()
                .stream()
                .filter(p -> p.getCategoria() == CategoriaProducto.REGULAR)
                .map(this::daoToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void updateProducto(Long id, ProductoDTO productoDTO) {
        ProductoDAO producto;
        producto = productoRepository.findById(id).orElseThrow(RuntimeException::new);
        producto.setDescripcion(productoDTO.getDescripcion());
        producto.setCategoria(productoDTO.getCategoria());
        producto.setPrecio(productoDTO.getPrecio());
        productoRepository.save(producto);
    }

    @Override
    public ProductoDTO createProducto(ProductoDTO productoDTO) {
        return daoToDto(productoRepository.save(dtoToDao(productoDTO)));
    }

    @Override
    public void deleteProducto(Long id) {
        productoRepository.deleteById(id);
    }

    private ProductoDTO daoToDto(ProductoDAO productoDAO){
        return new ProductoDTO(
                productoDAO.getId(),
                productoDAO.getDescripcion(),
                productoDAO.getCategoria(),
                productoDAO.getPrecio()
        );
    }

    private ProductoDAO dtoToDao(ProductoDTO productoDTO){
        ProductoDAO producto = new ProductoDAO();
        producto.setId(productoDTO.getId());
        producto.setCategoria(productoDTO.getCategoria());
        producto.setPrecio(productoDTO.getPrecio());
        producto.setDescripcion(productoDTO.getDescripcion());
        return producto;
    }
}
