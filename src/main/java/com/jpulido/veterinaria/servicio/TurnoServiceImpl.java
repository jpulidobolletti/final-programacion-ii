package com.jpulido.veterinaria.servicio;

import com.jpulido.veterinaria.modelo.dao.TurnoDAO;
import com.jpulido.veterinaria.modelo.dao.enums.Dia;
import com.jpulido.veterinaria.modelo.dto.TurnoDTO;
import com.jpulido.veterinaria.respositorio.TurnoRepository;
import com.jpulido.veterinaria.respositorio.VeterinarioRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurnoServiceImpl implements TurnoService {

    private final TurnoRepository turnoRepository;
    private final VeterinarioRepository veterinarioRepository;

    public TurnoServiceImpl(TurnoRepository turnoRepository, VeterinarioRepository veterinarioRepository) {
        this.turnoRepository = turnoRepository;
        this.veterinarioRepository = veterinarioRepository;
    }

    @Override
    public TurnoDTO findTurnoById(Long id) {
        return daoToDto(turnoRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    @Override
    public List<TurnoDTO> findAllTurnos() {
        return turnoRepository.findAll()
                .stream()
                .map(this::daoToDto)
                .collect(Collectors.toList());
    }

    @Override
    public boolean updateTurno(Long id, TurnoDTO turnoDTO) {
        TurnoDAO turno;
        turno = turnoRepository.findById(id).orElseThrow(RuntimeException::new);
        turno.setId(turnoDTO.getId());
        turno.setAnimal(turnoDTO.getAnimal());
        turno.setNombreDuenio(turnoDTO.getNombreDuenio());
        turno.setNombreMascota(turnoDTO.getNombreMascota());
        turno.setNumeroContacto(turnoDTO.getNumeroContacto());
        turno.setFecha(turnoDTO.getFecha());
        turno.setVeterinario(veterinarioRepository.findById(turnoDTO.getVeterinario()).orElseThrow(RuntimeException::new));
        //check veterinario especialidad con animal turno
        if(!turno.getVeterinario().getEspecialidades().contains(turno.getAnimal()) ){
            return false;
        }

        if(noEsDiaAtiende(turno)) {
            return false;
        }
        //Check para turno ocupado
        List<TurnoDTO> turnosAnteriores = turnoRepository.findAll()
                .stream()
                .filter(t -> t.getVeterinario().getUsuario().equals(turnoDTO.getVeterinario()) &&
                        t.getFecha().equals(turnoDTO.getFecha()) )
                .map(this::daoToDto)
                .collect(Collectors.toList());

        if(!turnosAnteriores.isEmpty()) {
            return false;
        }

        turnoRepository.save(turno);
        return true;
    }

    @Override
    public boolean createTurno(TurnoDTO turnoDTO) {
        TurnoDAO turno = dtoToDao(turnoDTO);
        if(!turno.getVeterinario().getEspecialidades().contains(turno.getAnimal()) ){
            return false;
        }

        if(noEsDiaAtiende(turno)) {
            return false;
        }

        //Check para turno ocupado
        List<TurnoDTO> turnosAnteriores = turnoRepository.findAll()
                .stream()
                .filter(t -> t.getVeterinario().getUsuario().equals(turnoDTO.getVeterinario()) &&
                        t.getFecha().equals(turnoDTO.getFecha()) )
                .map(this::daoToDto)
                .collect(Collectors.toList());

        if(!turnosAnteriores.isEmpty()) {
            return false;
        }

        turnoRepository.save(turno);

        return true;
    }

    private boolean noEsDiaAtiende(TurnoDAO turno) {
        Dia dia;
        switch (turno.getFecha().getDayOfWeek()){
            case MONDAY:
                dia = Dia.LUNES;
                break;
            case TUESDAY:
                dia = Dia.MARTES;
                break;
            case WEDNESDAY:
                dia = Dia.MIERCOLES;
                break;
            case THURSDAY:
                dia = Dia.JUEVES;
                break;
            case FRIDAY:
                dia = Dia.VIERNES;
                break;
            case SATURDAY:
                dia = Dia.SABADO;
                break;
            case SUNDAY:
            default:
                return true;
        }
        return !turno.getVeterinario().getDiasAtiende().contains(dia);
    }

    @Override
    public void deleteTurno(Long id) {
        turnoRepository.deleteById(id);
    }

    private TurnoDTO daoToDto(TurnoDAO turnoDAO){
        return new TurnoDTO(
                turnoDAO.getId(),
                turnoDAO.getNombreMascota(),
                turnoDAO.getNombreDuenio(),
                turnoDAO.getAnimal(),
                turnoDAO.getNumeroContacto(),
                turnoDAO.getVeterinario().getUsuario(),
                turnoDAO.getFecha()
        );
    }

    private TurnoDAO dtoToDao(TurnoDTO turnoDTO){
        TurnoDAO turno = new TurnoDAO();
        turno.setId(turnoDTO.getId());
        turno.setAnimal(turnoDTO.getAnimal());
        turno.setNombreDuenio(turnoDTO.getNombreDuenio());
        turno.setNombreMascota(turnoDTO.getNombreMascota());
        turno.setNumeroContacto(turnoDTO.getNumeroContacto());
        turno.setFecha(turnoDTO.getFecha());
        turno.setVeterinario(veterinarioRepository.findById(turnoDTO.getVeterinario()).orElseThrow(RuntimeException::new));
        return turno;
    }
}


